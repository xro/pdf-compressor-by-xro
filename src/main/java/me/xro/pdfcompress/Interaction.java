package me.xro.pdfcompress;

import java.util.Scanner;

/**
*
* @author xro
* https://twitter.com/xro
*/
public class Interaction {
	
	private Scanner scanner;
	
	
	
	public Interaction() {
		if (this.scanner == null) {
			Scanner scann = new Scanner(System.in);
			this.scanner = scann;
		}
	}

	public void showGreetings() {
		
		System.out.println("Hi! I am a PDF compressor by XRO. Press enter to start.. or 'h' fo help.>");
		System.out.print("> ");
		String input = scanner.nextLine();
		if (input.equalsIgnoreCase("h")){
			showHelp();
			System.out.println("Press Enter to end the program");
			System.out.print("> ");
			input = scanner.nextLine();
			return;
		}
		System.out.print("running..\n");
	}
	
	public void showHelp(){
		System.out.println("");
		System.out.println("Instructions:");
		System.out.println("1) Create a folder c:\\pdf");
		System.out.println("2) Put all your large pdf-files in there.");
		System.out.println("3) Run the programm. Es werden 2 Unterordner erstellt: \"pdf\" und \"png\". Im pdf-Ordner findest komprimierte pdf-Dateien. Im png-Ordner komprimierte Bilder.");
		System.out.println("Important: every run rewrites the files");
	}
	
	public void endInteraction() {
		System.out.println("");
		System.out.println("Done! :) Press something to close me.");
		String username = scanner.nextLine();
	}
	
	

}
