package me.xro.pdfcompress;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.jhlabs.image.ReduceNoiseFilter;

/**
*
* @author xro
* https://twitter.com/xro
*/
public class Runner {

	public static void main(String[] args) throws NumberFormatException, IOException {
		
		Interaction interactObj = new Interaction();
		interactObj.showGreetings();
		
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);

		String dir = "C:/pdf";
		int maxSize = 2000;
		
		Helper.initWorkingDir(dir);

		File folder = new File(dir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile() && FilenameUtils.getExtension(listOfFiles[i].getName()).equalsIgnoreCase("pdf")) {
				handlPDF(listOfFiles[i], maxSize);
				
			}
		}
		
		interactObj.endInteraction();
		

	}

	// process pdf file
	public static void handlPDF(File file, int maxSize) throws InvalidPasswordException, IOException {
		
		String message = "" + file.getName() + " " + Helper.getFileSize(file.getPath()) + " ---> ";
		//System.out.println("ORIGINAL SIZE: " + getFileSize(file.getPath()));
		PDDocument document = PDDocument.load(file);

		PDFRenderer pdfRenderer = new PDFRenderer(document);
		int pageCounter = 0;
		PDDocument newDoc = new PDDocument();
		
		char[] animationChars = new char[]{'|', '/', '-', '\\'};
		int i = 1;
		
		for (PDPage page : document.getPages()) {
			System.out.print("Processing: " + animationChars[i++ % 4] + "\r");
			// note that the page number parameter is zero based
			BufferedImage bim = pdfRenderer.renderImageWithDPI(pageCounter, 600, ImageType.RGB);
			// System.out.println ("ORIGINAL Width: " + bim.getWidth() + "Height: " + bim.getHeight());

			// resize
			double resizeFactor = calculateResizeRate(bim.getWidth(), bim.getHeight(), maxSize);
			int w = (int) (bim.getWidth() * resizeFactor);
			int h = (int) (bim.getHeight() * resizeFactor);
			// System.out.println ("COMPRESSED Width: " + w + " Height: " + h);
			BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
			Graphics g = newImage.createGraphics();
			g.drawImage(bim, 0, 0, w, h, null);
			g.dispose();
			
			//reduce noise
			ReduceNoiseFilter rn = new ReduceNoiseFilter();
			newImage = rn.filter(newImage, null);
			
			// sharpen image (reduces size for PNG, somehow)
			Kernel kernel = new Kernel(3, 3, new float[] { -1, -1, -1, -1, 9, -1, -1, -1, -1 });
			BufferedImageOp op = new ConvolveOp(kernel);
			newImage = op.filter(newImage, null);
			
			//threshold image. removes noise
			newImage = thresholdImage(newImage, 5);
			
			System.out.print("Processing: " + animationChars[i++ % 4] + "\r");

			
			String newFileName = file.getParent() + "\\png\\" + file.getName() + (pageCounter++) + ".png";
			saveImageAsPNG(newImage, new FileOutputStream(newFileName));
			//saveImageAsJPEG(newImage, new FileOutputStream(newFileName), 10);

			// this saves to jpg, via PDFBOX, but JPG is generaly bad for text.
			// use PNG instead
			// OutputStream out = new FileOutputStream(newFileName);
			// ImageIOUtil.writeImage(newImage, "jpg", out, 300, compression);

			// write compressed pdf

			PDRectangle rec = new PDRectangle(w, h);
			PDPage npage = new PDPage(rec);
			PDPageContentStream contentStream = new PDPageContentStream(newDoc, npage);
			PDImageXObject img = LosslessFactory.createFromImage(newDoc, newImage);
			//PDImageXObject img = JPEGFactory.createFromStream(newDoc, new FileInputStream(newFileName));
			contentStream.drawImage(img, 0, 0);
			contentStream.close();
			newDoc.addPage(npage);

//			System.out.println("COMPRESSED SIZE: " + getFileSize(newFileName));

		}
		
		
		
		document.close();
		String newPDF = file.getParent() + "\\pdf\\" + file.getName();
		newDoc.save(newPDF);
					
		newDoc.close();
		
		message += Helper.getFileSize(newPDF);
		
		
		File newPdfFile = new File(newPDF);
		
		
		//if result file is bigger than source file -> copy source file to pdf/pdf instead
		if (newPdfFile.length() >= file.length()) {
			message += " TOO BIG! REVERTED!";
			newPdfFile.delete();
			FileUtils.copyFile(file, new File(newPDF));
			
		}
		
		System.out.println(message);
		
	}
	

	// lets calculate maximum resize rate for a givin maximum size of width or
	// heigh. Resized image will not exceed the max size.
	public static double calculateResizeRate(int w, int h, int maxSize) {

		// if already small, nothing to resize
		if (w <= maxSize && h <= maxSize)
			return 1;

		if (h > w) {
			double rate = (double) maxSize / h;
			return rate;

		} else {
			double rate = (double) maxSize / w;
			return rate;
		}
	}



	// write jpg
	public static void saveImageAsJPEG(BufferedImage image, OutputStream stream, int qualityPercent)
			throws IOException {
		if ((qualityPercent < 0) || (qualityPercent > 100)) {
			throw new IllegalArgumentException("Quality out of bounds!");
		}
		float quality = qualityPercent / 100f;
		ImageWriter writer = null;
		Iterator iter = ImageIO.getImageWritersByFormatName("jpg");
		if (iter.hasNext()) {
			writer = (ImageWriter) iter.next();
		}
		ImageOutputStream ios = ImageIO.createImageOutputStream(stream);
		writer.setOutput(ios);
		ImageWriteParam iwparam = new JPEGImageWriteParam(Locale.getDefault());
		iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwparam.setCompressionQuality(quality);
		writer.write(null, new IIOImage(image, null, null), iwparam);
		ios.flush();
		writer.dispose();
		ios.close();
	}

	// write png
	public static void saveImageAsPNG(BufferedImage image, OutputStream stream) throws IOException {
		ImageIO.write(image, "png", stream);
	}

	public static void saveImageAsGIF(BufferedImage image, OutputStream stream) throws IOException {
		ImageIO.write(image, "gif", stream);
	}


	public static BufferedImage thresholdImage(BufferedImage image, int threshold) {
	    BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
	    result.getGraphics().drawImage(image, 0, 0, null);
	    WritableRaster raster = result.getRaster();
	    int[] pixels = new int[image.getWidth()];
	    for (int y = 0; y < image.getHeight(); y++) {
	        raster.getPixels(0, y, image.getWidth(), 1, pixels);
	        for (int i = 0; i < pixels.length; i++) {
	            if (pixels[i] < threshold) pixels[i] = 0;
	            else pixels[i] = 255;
	        }
	        raster.setPixels(0, y, image.getWidth(), 1, pixels);
	    }
	    return result;
	}


}
