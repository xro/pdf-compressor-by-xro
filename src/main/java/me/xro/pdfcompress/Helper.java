package me.xro.pdfcompress;

import java.io.File;

/**
*
* @author xro
* https://twitter.com/xro
*/
public class Helper {
	
	
	//create the working directories
	public static void initWorkingDir(String dir){
		
		File file = new File(dir+"/png");
		if (!file.exists()) {
			System.out.println("creating png dir");
		    file.mkdir();
		}
		file = new File(dir+"/pdf");
		if (!file.exists()) {
			System.out.println("creating pdf dir");
		    file.mkdir();
		}
		
	}
	
	public static String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public static String getFileSize(String fileName) {
		File file = new File(fileName);
		return humanReadableByteCount(file.length(), false);

	}

}
